************
Service APIs
************

Base Service Class
==================

.. automodule:: enoslib.service.service
    :members:
    :undoc-members:
    :show-inheritance:

Conda & Dask
=============

Conda & Dask Service Class
--------------------------


.. automodule:: enoslib.service.conda.conda
    :members:
    :undoc-members:
    :show-inheritance:


Docker
======

Docker Service Class
--------------------


.. automodule:: enoslib.service.docker.docker
    :members:
    :undoc-members:
    :show-inheritance:

Dstat (monitoring)
==================

Dstat Service Class
--------------------


.. automodule:: enoslib.service.dstat.dstat
    :members:
    :undoc-members:
    :show-inheritance:

Locust (Load generation)
========================

Locust Service Class
------------------------

.. automodule:: enoslib.service.locust.locust
    :members:
    :undoc-members:
    :show-inheritance:

Monitoring
==========

Monitoring Service Class
------------------------

.. _monitoring:


.. automodule:: enoslib.service.monitoring.monitoring
    :members:
    :undoc-members:
    :show-inheritance:


Network Emulation (Netem & SimpleNetem)
=======================================

.. _netem:

Netem & SimpleNetem Service Class
---------------------------------

To enforce network constraint, |enoslib| provides two different services: the
Netem Service and the SimpleNetem Service. The former tends to be used when
heterogeneous constraints are required between your hosts while the latter
can be used to set homogeneous constraints between your hosts.

Netem and SimpleNetem Class
***************************

.. automodule:: enoslib.service.netem.netem
    :members:
    :undoc-members:
    :show-inheritance:


Skydive
=======

.. _skydive:

Skydive Service Class
---------------------


.. automodule:: enoslib.service.skydive.skydive
    :members:
    :undoc-members:
    :show-inheritance: