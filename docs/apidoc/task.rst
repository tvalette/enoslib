.. _task:

********
Task API
********

.. automodule:: enoslib.task
    :members:
    :undoc-members: